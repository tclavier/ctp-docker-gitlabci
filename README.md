# Contrôle TP (3 heures)

## Sujet

Vous trouverez dans ce dépot un web service en java. Pour le tester vous pouvez poster un XML ou en JSON sur l'URL `/interval/` un objet comme : 

```json
{
    "start":{
        "day":"2", 
        "month":"2", 
        "year":"2020"}, 
    "end":{
        "day":"4",
        "month":"2", 
        "year":"2020"
    }
}
```

La réponse devrait être :

```json
{
    "days":2,
    "errorMessage":""
}
```

## Première étape

* Commencer par créer un projet `ctp` dans le groupe https://gitlab.com/univlille/2022-lc512c-deploiement-cloud-cgir/VOTRE-GROUPE/
* Cloner ce projet chez vous `git clone URL.git`
* Dans le projet vide que vous venez de cloner, décompresser l'archive du projet courant : https://gitlab.com/univlille/2022-lc512c-deploiement-cloud-cgir/sujets/ctp/-/archive/master/ctp-master.tar.gz
* Vous devez avoir le fichier `pom.xml` à la racine de votre dossier

⚠️ Faire des commits très régulièrement, toutes les 5 ou 10 minutes maximum ⚠️


## Dockerfile (7 points)

* Écrire le `Dockerfile` qui permettra de déployer cette application.
* Écrire le script shell `test.sh` qui vous permettra de tester le container localement.

Voici quelques éléments qui devraient vous aider : 

* Commande pour construire le projet : `mvn package`
* Commande pour télécharger les dépendances : `mvn dependency:go-offline`
* Commande pour lancer les tests : `mvn test`
* Commande pour lancer le service après compilation: `java -jar target/ctp-1.0-jar-with-dependencies.jar`
* Pour dire à maven d'utiliser un proxy, ajouter un fichier `~/.m2/settings.xml` avec ce contenu suivant
```xml 
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 
                    http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <proxies>
        <proxy>
            <id>proxy</id>
            <active>true</active>
            <protocol>http</protocol>
            <host>cache.univ-lille1.fr</host>
            <port>3128</port>
        </proxy>
    </proxies>
</settings>
```
* Le service écoute sur le port 8080 de toutes les interfaces en http
* Pour connaitre l'état du service il suffit de visiter l'URL : `/health/`
* Pour changer le nom du service il suffit de rajouter un fichier `/etc/interval/config.properties` avec le contenu `service.name=Le nouveau nom de service`
* Pour faire face à de forte charges, il est possible de déclarer un serveur redis pour mettre en cache les réponses avec la variable d'environement REDIS_SERVER

Demandes complémentaires : 

* Optimiser le temps de construction du container en considérant que les fréquences de modification sont les suivantes : 
    * Les fichiers .java sont modifiés plusieurs fois par jour.
    * Le fichier `pom.xml` n'est modifié que rarement.
    * Les modifications systemes sont encore plus rare

## CI/CD (7 points)

* Écrire le `.gitlab-ci.yml` permettant de construire le container et de l'archiver dans la registry gitlab
* Ajouter une étape de CI/CD qui execute les tests avant la construction du container
* À chaque pose de tag dans git, publier un container avec le même tag

Demandes complémentaires :

* Ajouter le badge de test coverage sur votre projet
* Publiez à chaque build l'ensemble des résultats de tests dans l'onglet "Tests" du pipeline gitlab

## Runtime (6 points)

* Décrire dans un fichier `HOWTO.md` la ligne de commande à utiliser pour lancer le container construit par gitlab-ci. (1 point)
* Ajouter un fichier `config/config.properties` pour mettre le nom du service à `Service CTP LP CGIR` et décrire dans le fichier `HOWTO.md` la ligne de commande à utiliser pour utiliser ce fichier de propertie (2 points)
* Décrire dans le fichier `HOWTO.md` les lignes de commande à utiliser pour lancer dans un même réseau votre container et un serveur redis. (3 points)

# Phase de correction (1 heure)

Vous avez 20 min pour corriger le TP de votre camarade juste avant vous dans la liste suivante : 

* Varrier Gael
* Toursel Gabriel
* Delehedde Louka
* Salingue Logan
* Wuilmart Jade
* Salah Josyan
* Righi Lucas
* Turpin Theo
* Devauchelle Loic
* Morel Bastien
* Hamouchi Amjed
* Debieb Ryan
* Bonnet Tanguy
* Nowacki Hugo
* Masclet Mathieu
* Clinckx Hugo
* Ait Mouhou Sofian
* Lernould Baptiste
* Desmarets Simon

Pour se faire, ouvrir le repository git correspondant et ajoutez à la racine un fichier `nom.prenom.md` avec votre nom et votre prénom, composé de la structure suivante : 

```
* blabla c'est inadmissible de déclarer le proxy dans le Dockerfile -5
* blabla il manque truc -1
* blabla c'est super d'avoir utiliser machin +1

| Question   | Note |
|------------|------|
| Dockerfile |      |
| CI/CD      |      |
| Runtime    |      |
| Total      |      |
```


## Barème de correction

Comme une dictée, la copie démarre avec 20 points. Chaque erreur enlève des points, chaque élément bonus en ajoute.

Pour toutes les questions je vous invite à scruter avec minutie l'historique des commits afin d'y vérifier l'avancement régulier.

* `git log --oneline --decorate --graph --all --format="%h %Cblue%ad% %C(auto) %s"` permet de lister tous les commits avec l'heure et le commentaire
* `for commit in $(git log --reverse --pretty=format:%H); do git checkout $commit; git show --stat $commit; read; done` permet de rejouer la liste de tous les commit 1 à 1 en affichant quelques éléments. Pour passer d'un commit à l'autre, il faut sortir du git show et valider avec la touche entrée.
* `git checkout master` permet de revenir à l'état dans le gitlab.

Toute tentative de triche, de corruption ou de trafic d'influence sera sanctionné d'un 0 pour tous les acteurs directement impliqués.

La qualité de vos corrections influerons vos notes.

Testez avec le script proposé et/ou votre propre script la bonne réponse aux questions.

Stay classy.

### Dockerfile

Les plus :

* Utiliser la directive healthcheck
* Utiliser du multistage
* Le script de test est lancé depuis un container
* Les demandes complémentaires ont été prises en compte.

Les moins : 

* Déclarer le proxy dans le Dockerfile
* Ne pas ignorer le dossier target
* Installer des composants non indispensables
* Recompiler le programme au lancement
* Le non respect de toutes les bonnes pratiques vues en TP.
* Séparer dans des layers séparés Update et Install
* Ne pas nettoyer un layer avant de passer au suivant (par exemple avec apt-get clean)
* La non réponse à une des demandes

### CI/CD

Les plus : 

* Un "stage" de test, un "stage" qualité et un autre de build
* Utiliser le cache de gitlab
* Création d'un tag flotant "stable"
* Les demandes complémentaires ont été prises en compte.

Les moins : 

* La non réponse à une des demandes
* latest n'est pas que sur la branche master
* Duplication de code
* Non respect des bonnes pratiques
* Tout dans 1 "stage"
* Non utilisation des variables gitlab
* Utilisation du template gitlab.com sans l'avoir compris.

## Runtime (6 points)

Les plus : 

* L'utilisation d'un docker-compose en plus du fichier `HOWTO.md`.

Les moins : 

* La non réponse à une des demandes
* Le non fonctionnement d'une des commande décrite.

# Astuces 

* https://gitlab.com/tclavier/tdd-bdd-4-ba/
