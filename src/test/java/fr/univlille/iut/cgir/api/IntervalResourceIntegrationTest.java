package fr.univlille.iut.cgir.api;


import org.glassfish.jersey.test.JerseyTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IntervalResourceIntegrationTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new Api();
    }

    @BeforeAll
    public void before() throws Exception {
        super.setUp();
    }

    @AfterAll
    public void after() throws Exception {
        super.tearDown();
    }

    @Test
    public void should_call_xml_web_service() {
        BetweenXml between = BetweenXml.from(DateXml.from("14", "01", "2006"), DateXml.from("21", "01", "2006"));
        Entity<BetweenXml> betweenEntity = Entity.entity(between, MediaType.APPLICATION_XML);
        String interval = target("/interval/").request().accept(MediaType.APPLICATION_XML).post(betweenEntity).readEntity(String.class);
        assertEquals("" +
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<intervalXml>" +
                "<days>7</days>" +
                "<errorMessage></errorMessage>" +
                "</intervalXml>", interval);
    }

    @Test
    public void should_call_json_web_service() {
        BetweenXml between = BetweenXml.from(DateXml.from("14", "01", "2006"), DateXml.from("21", "01", "2006"));
        Entity<BetweenXml> betweenEntity = Entity.entity(between, MediaType.APPLICATION_JSON);
        String interval = target("/interval/").request().accept(MediaType.APPLICATION_JSON).post(betweenEntity).readEntity(String.class);
        assertEquals("{\"days\":7,\"errorMessage\":\"\"}" , interval);
    }

    @Test
    public void should_call_web_service_with_object() {
        BetweenXml between = BetweenXml.from(DateXml.from("14", "01", "2006"), DateXml.from("21", "01", "2006"));
        Entity<BetweenXml> betweenEntity = Entity.entity(between, MediaType.APPLICATION_XML);
        IntervalXml interval = target("/interval/").request().post(betweenEntity).readEntity(IntervalXml.class);
        assertEquals(IntervalXml.from("", 7), interval);
    }
}