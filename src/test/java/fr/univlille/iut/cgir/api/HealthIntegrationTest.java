package fr.univlille.iut.cgir.api;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.ws.rs.core.Application;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HealthIntegrationTest extends JerseyTest {
    @Override
    protected Application configure() {
        return new Api();
    }

    @BeforeAll
    public void before() throws Exception {
        super.setUp();
    }

    @AfterAll
    public void after() throws Exception {
        super.tearDown();
    }

    @Test
    public void should_call_health_url() {
        String body = target("/health/").request().get().readEntity(String.class);
        assertEquals("Interval Service", body.split("\n")[0]);
    }

    @Test
    public void should_show_configuration_file() {
        String body = target("/health/").request().get().readEntity(String.class);
        String configLine = "";
        for (String line : body.split("\n")) {
            if (line.toLowerCase(Locale.ROOT).contains("properties")) configLine = line;
        }
        assertEquals("* Properties : default",configLine);
    }

}
