package fr.univlille.iut.cgir.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class ConfigurationTest {
    @Test
    void should_have_a_default_configuration() {
        Configuration configuration = Configuration.getInstance();
        Assertions.assertTrue(configuration.useDefault());
        Configuration.resetInstance();
    }

    @Test
    void should_use_specific_configuration_file() throws IOException {
        Configuration configuration = Configuration.getInstance();
        Files.write(Paths.get("/tmp/test.properties"), "toto=titi".getBytes(StandardCharsets.UTF_8));
        configuration.reload("/tmp/test.properties");
        Assertions.assertFalse(configuration.useDefault());
        Configuration.resetInstance();
    }

    @Test
    void should_update_service_name () throws IOException {
        Configuration configuration = Configuration.getInstance();
        Files.write(Paths.get("/tmp/srv_name.properties"), "service.name=The best service".getBytes(StandardCharsets.UTF_8));
        configuration.reload("/tmp/srv_name.properties");
        Assertions.assertEquals("The best service", configuration.getServiceNane());
        Configuration.resetInstance();
    }
    @Test
    void should_ignore_bad_service_name () throws IOException {
        Configuration configuration = Configuration.getInstance();
        Files.write(Paths.get("/tmp/srv_name.properties"), "service.nom=The best service".getBytes(StandardCharsets.UTF_8));
        configuration.reload("/tmp/srv_name.properties");
        Assertions.assertEquals("Interval Service", configuration.getServiceNane());
        Configuration.resetInstance();
    }
}