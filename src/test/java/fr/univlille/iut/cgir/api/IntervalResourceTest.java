package fr.univlille.iut.cgir.api;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class IntervalResourceTest {
    @Test
    void should_take_a_between_in_input_and_return_an_interval() {
        DateXml start = DateXml.from("23", "02", "1983");
        DateXml end = DateXml.from("24", "02", "1983");
        BetweenXml betweenXml = BetweenXml.from(start, end);

        IntervalXml expected = IntervalXml.from(1L);

        IntervalResource intervalResource = new IntervalResource();
        assertEquals(expected, intervalResource.computeInterval(betweenXml));
    }

    @Test
    void should_compare_IntervalXml() {
        IntervalXml expected = IntervalXml.from("message", 1L);
        IntervalXml actual1 = IntervalXml.from("message", 1);
        IntervalXml actual2 = IntervalXml.from("message 2", 2);
        assertEquals(expected, actual1);
        assertNotEquals(expected, actual2);
    }

    @Test
    void should_build_dateXml_from_string() {
        DateXml date = DateXml.from("24", "02", "1983");
        assertEquals("24", date.getDay());
        assertEquals("02", date.getMonth());
        assertEquals("1983", date.getYear());
    }

    @Test
    void should_convert_dateXml_to_domain() {
        DateXml dateXml = DateXml.from("09", "12", "2006");
        LocalDate actual = dateXml.toLocalDate();
        LocalDate expected = LocalDate.of(2006, 12, 9);
        assertEquals(expected, actual);
    }

    @Test
    void should_really_build_from_factory() {
        DateXml start = DateXml.from("21", "01", "2006");
        DateXml end = DateXml.from("14", "01", "2006");
        BetweenXml between = BetweenXml.from(start, end);
        assertEquals("14", between.getEnd().getDay());
        assertEquals("2006", between.getStart().getYear());
    }

    @Test
    void should_have_an_error_message_when_date_are_in_wrong_order() {
        DateXml start = DateXml.from("21", "01", "2006");
        DateXml end = DateXml.from("14", "01", "2006");
        BetweenXml between = BetweenXml.from(start, end);

        IntervalXml expected = IntervalXml.from("End date must be after start date");
        IntervalResource intervalResource = new IntervalResource();
        assertEquals(expected, intervalResource.computeInterval(between));
    }

    @Test
    void should_have_an_error_message_when_start_date_is_invalid() {
        DateXml start = DateXml.from("45", "01", "2006");
        DateXml end = DateXml.from("14", "01", "2006");
        BetweenXml between = BetweenXml.from(start, end);

        IntervalXml expected = IntervalXml.from("Start date must be valid");
        IntervalResource intervalResource = new IntervalResource();
        assertEquals(expected, intervalResource.computeInterval(between));
    }

    @Test
    void should_have_an_error_message_when_end_date_is_invalid() {
        DateXml start = DateXml.from("15", "01", "2006");
        DateXml end = DateXml.from("29", "02", "2006");
        BetweenXml between = BetweenXml.from(start, end);

        IntervalXml expected = IntervalXml.from("End date must be valid");
        IntervalResource intervalResource = new IntervalResource();
        assertEquals(expected, intervalResource.computeInterval(between));
    }
}
