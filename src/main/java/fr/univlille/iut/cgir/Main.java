package fr.univlille.iut.cgir;

import fr.univlille.iut.cgir.api.Api;
import fr.univlille.iut.cgir.api.Configuration;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;

import java.io.IOException;
import java.net.URI;


public class Main {
    public static final String BASE_URI = "http://0.0.0.0:8080/";

    public static void main(String[] args) throws IOException {
        Configuration configuration = Configuration.getInstance();
        String redis = System.getProperty("REDIS_SERVER");
        if (redis != null)
            configuration.setRedisHost(redis);
        final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), new Api());
    }
}
