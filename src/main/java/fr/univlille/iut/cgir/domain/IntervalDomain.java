package fr.univlille.iut.cgir.domain;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class IntervalDomain {
    public static long between(LocalDate startDate, LocalDate endDate) {
        if (startDate.isAfter(endDate)) throw new InvalidParameterException();
        return ChronoUnit.DAYS.between(startDate, endDate);
    }
}
