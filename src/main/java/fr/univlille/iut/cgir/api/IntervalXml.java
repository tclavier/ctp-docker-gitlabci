package fr.univlille.iut.cgir.api;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IntervalXml {
    private long days;
    private String errorMessage;

    public static IntervalXml from(long days) {
        return IntervalXml.from("", days);
    }

    public static IntervalXml from(String message, long i) {
        IntervalXml intervalXml = new IntervalXml();
        intervalXml.errorMessage = message;
        intervalXml.days = i;
        return intervalXml;
    }

    public static IntervalXml from(String s) {
        return IntervalXml.from(s, 0);
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "IntervalXml{" +
                "days=" + days +
                ", message='" + errorMessage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntervalXml that = (IntervalXml) o;

        if (days != that.days) return false;
        return errorMessage != null ? errorMessage.equals(that.errorMessage) : that.errorMessage == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (days ^ (days >>> 32));
        result = 31 * result + (errorMessage != null ? errorMessage.hashCode() : 0);
        return result;
    }
}
