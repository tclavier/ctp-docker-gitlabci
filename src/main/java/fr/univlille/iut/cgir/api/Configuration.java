package fr.univlille.iut.cgir.api;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    public static final String NO_REDIS_SERVER = "no redis server";
    private static Configuration instance;
    private boolean isDefault;
    private String redisHost = NO_REDIS_SERVER;
    private String serviceName = "Interval Service";


    private Configuration() {
    }

    public static Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
            instance.reload("/etc/interval/config.properties");
        }
        return instance;
    }

    static void resetInstance() {
        instance = null;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public boolean useDefault() {
        return isDefault;
    }

    public void reload(String s) {
        Properties props = new Properties();
        try (FileInputStream in = new FileInputStream(s)) {
            props.load(in);
            if (props.containsKey("service.name")) this.serviceName = props.getProperty("service.name");
            this.isDefault = false;
        } catch (IOException e) {
            this.isDefault = true;
        }
    }

    public String getServiceNane() {
        return serviceName;
    }
}
