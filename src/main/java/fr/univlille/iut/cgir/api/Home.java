package fr.univlille.iut.cgir.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.TEXT_HTML)
public class Home {
    @GET
    public String health() {
        return "<html>" +
                "<body>" +
                "   <h1>"+Configuration.getInstance().getServiceNane()+"</h1>" +
                "</body>" +
                "</html>";
    }
}
